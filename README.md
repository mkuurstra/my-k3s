# My k3s

## Design

### Infrastructure services

![Infrastructure services overview](docs/media/my-k3s-Infra-Services.drawio.png)

#### Observability

- **Visualization**: [Grafana](https://grafana.com/docs/grafana/latest/introduction/)
- **Alert Dashboard**: [Alerta](https://docs.alerta.io/)
- **Alert Manager**: [Alertmanager](https://prometheus.io/docs/alerting/latest/alertmanager/)
- **Metrics**: [Prometheus](https://prometheus.io/docs/introduction/overview/)
- **Metrics aggregation**: [Thanos](https://thanos.io/tip/thanos/quick-tutorial.md/) (Planned)
- **Log shipping**: [Vector](https://vector.dev/docs/about/what-is-vector/)
- **Log aggregation**: [Loki](https://grafana.com/docs/loki/latest/get-started/overview/)

#### Service-Delivery

- **Continuous Delivery**: [Argo CD](https://argo-cd.readthedocs.io/en/stable/)
- **Secret management**: [1Password Connect Kubernetes Operator](https://github.com/1Password/onepassword-operator)
- **Certificate management**: [cert-manager](https://cert-manager.io/docs/) (Using Cloudflare API & Lets Encrypt)
- **SSO**: [Authentik](https://goauthentik.io/docs/)
- **Registry**: [Harbor](https://goharbor.io/docs/)
- **Memory Data Store**: [Redis](https://redis.io/docs/about/)

#### Connectivity

- **Networking**: [Calico](https://docs.tigera.io/calico/latest/about/)
- **DNS**: [CoreDNS](https://coredns.io/manual/what/)
- **Load-balancer**: [MetalLB](https://metallb.universe.tf/)
- **Reverse proxy**: [Traefik](https://doc.traefik.io/traefik/)

#### Storage

- **Storage**: [Longhorn](https://longhorn.io/docs/latest/what-is-longhorn/)
- **Backup**: [Velero](https://velero.io/docs/v1.8/)
- **Off-site backup**: S3 capable API

#### Maintenance

- **Node Upgrades**: [system-upgrade-controller](https://github.com/rancher/system-upgrade-controller)
- **Scheduled reboots**: [Kured](https://kured.dev/docs/)
- **Service Upgrades**: [Renovate](https://docs.renovatebot.com/)

### Infrastructure

![Infrastructure](./docs/media/infrastructure.drawio.png)

#### Controllers

| Name     | Comment               |
| -------- | --------------------- |
| s-mini03 | Main controller       |
| s-pi01   | Secondary controller |

> :warning: **WARNING**
>
> The NGINX reverse proxy and secondary controller is mainly to test upgrades before performing them on the main controller. This was implemented after a failed upgrade but I wouldn't advise on implementing this, I will most probably get rid of this someday.

#### Agents

| Name     |
| -------- |
| s-mini01 |
| s-mini02 |
| s-lap01  |

## Installation

> :warning: **WARNING**
>
> Installation proces below works pretty well, only thing I'm not confident about is the way I manage CoreDNS in the Post-Installation. Be prepared to run into trouble with DNS.

### Mariadb

<https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-ubuntu-22-04>

```bash
sudo apt update
sudo apt install mariadb-server
sudo mysql_secure_installation
```

Open console with `mysql -u root`, update `password` below and insert commands below:

```sql
CREATE DATABASE k3s CHARACTER SET latin1 COLLATE latin1_swedish_ci;
CREATE USER 'k3s'@'192.168.2.%' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON k3s.* TO 'k3s'@'192.168.2.%';
FLUSH PRIVILEGES;
exit
```

Make Mariadb listen on public facing interface, edit `/etc/mysql/mariadb.conf.d/50-server.cnf` and edit the line with `bind-address`:

```bash
bind-address            = 0.0.0.0
```

And restart Mariadb:

```bash
sudo systemctl restart mariadb
```

### NGINX

<https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-22-04>

```bash
sudo apt update
sudo apt install nginx
```

Append the config below to `/etc/nginx/nginx.conf`

```bash
stream {
  upstream k3s_servers {
    server localhost:8443;
    server 192.168.2.5:6443;
  }

  server {
    listen 6443;
    proxy_pass k3s_servers;
  }
}
```

### K3s main controller

```bash
export K3S_DATASTORE_ENDPOINT='mysql://k3s:password@tcp(main_controller_ip)/k3s'
curl -sfL https://get.k3s.io | sh -s - server \
    --https-listen-port 8443 \
    --cluster-cidr=10.244.0.0/16 \
    --disable=traefik \
    --disable=servicelb \
    --flannel-backend none  \
    --disable-network-policy \
    --node-taint CriticalAddonsOnly=true:NoExecute \
    --write-kubeconfig-mode=644 \
    --tls-san main_controller_ip
```

Retrieve token:

```bash
sudo cat /var/lib/rancher/k3s/server/node-token
```

#### Install Calico

```bash
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.25.1/manifests/tigera-operator.yaml
```

Create a file `calico-custom-resources.yaml` with the following content:

```yaml
---
apiVersion: operator.tigera.io/v1
kind: Installation
metadata:
  name: default
spec:
  calicoNetwork:
    containerIPForwarding: "Enabled"
    ipPools:
    - blockSize: 26
      cidr: 10.244.0.0/16
      encapsulation: VXLANCrossSubnet
      natOutgoing: Enabled
      nodeSelector: all()
---

apiVersion: operator.tigera.io/v1
kind: APIServer 
metadata: 
  name: default 
spec: {}
```

And apply with:

```bash
kubectl create -f calico-custom-resources.yaml
```

### K3s secondary controller

```bash
export K3S_DATASTORE_ENDPOINT='mysql://k3s:password@tcp(main_controller_ip)/k3s'
TOKEN="token-from-above"
curl -sfL https://get.k3s.io | sh -s - server \
    --https-listen-port 8443 \
    --cluster-cidr=10.244.0.0/16 \
    --disable=traefik \
    --disable=servicelb \
    --flannel-backend=none \
    --disable-network-policy \
    --node-taint CriticalAddonsOnly=true:NoExecute \
    --write-kubeconfig-mode=644 \
    --tls-san main_controller_ip \
    --token=$TOKEN
```

### K3s agents

```bash
curl -sfL https://get.k3s.io | K3S_URL=https://main_controller_ip:6443 K3S_TOKEN=mynodetoken sh -
```

### Local kubectl and kubeconfig

- Install kubectl
- Copy content of `/etc/rancher/k3s/k3s.yaml` on the main controller
- Copy contents on local machine to `~/.kube/config`
- Update `server:` to main controller ip and port 6443

Example: `server: https://main_controller_ip:6443`

### 1Password Connect Kubernetes Operator

[Initial 1Password secret](./apps/1password/README.md#initial-secret)

### Argo CD

```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

kubectl apply -n argocd -f ./namespaces/argocd/charts/argocd/templates/application-argo-initial.yaml
```

If needed restore backups using Longhorn / Velero and then:

```bash
kubectl apply -n argocd -f ./namespaces/argocd/charts/argocd/templates/application-argo-remainder.yaml
```

## Resources

- <https://docs.technotim.live/posts/k3s-ha-install/>

## Post-Installation

### Disable k3s coredns

- `vim /etc/systemd/system/k3s.service`
- Add option: `--disable=coredns`
- `systemctl daemon-reload`
- `systemctl restart k3s`

Possibly Argo CD will not work to fix missing service, so need to manually add this manifest:

```yaml
---
# Source: coredns/charts/coredns/templates/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: kube-dns
  labels:
    app.kubernetes.io/managed-by: "Helm"
    app.kubernetes.io/instance: "coredns"
    helm.sh/chart: "coredns-1.26.0"
    k8s-app: coredns
    kubernetes.io/cluster-service: "true"
    kubernetes.io/name: "CoreDNS"
    app.kubernetes.io/name: coredns
spec:
  selector:
    app.kubernetes.io/instance: "coredns"
    k8s-app: coredns
    app.kubernetes.io/name: coredns
  clusterIP: 10.43.0.10
  ports:
  - {"name":"udp-53","port":53,"protocol":"UDP"}
  - {"name":"tcp-53","port":53,"protocol":"TCP"}
  type: ClusterIP
```

### Extra metrics k3s

Create `/etc/rancher/k3s/config.yaml`:

```shell
kube-controller-manager-arg:
- "address=0.0.0.0"
- "bind-address=0.0.0.0"
kube-proxy-arg:
- "metrics-bind-address=0.0.0.0"
kube-scheduler-arg:
- "address=0.0.0.0"
- "bind-address=0.0.0.0"
# Controller Manager exposes etcd sqllite metrics
etcd-expose-metrics: true
```

Restart k3s:

```shell
systemctl restart k3s
```
