# Installatie

Netwerken:

- Pods: 10.200.0.0/16
- Service: 10.201.0.0/16

## Installeer k3s op eerste controller

```bash
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.27.11+k3s1 sh -s - server \
    --service-cidr=10.201.0.0/16 \
    --disable=coredns \
    --disable=traefik \
    --disable=servicelb \
    --flannel-backend none  \
    --disable-network-policy \
    --disable-kube-proxy \
    --cluster-init \
    --cluster-dns=10.201.0.2 \
    --cluster-domain=k8s.kuurstra.com \
    --node-taint CriticalAddonsOnly=true:NoExecute \
    --data-dir=/var/lib/rancher/k3s \
    --tls-san=192.168.2.100 \
    --etcd-expose-metrics \
    --kube-controller-manager-arg bind-address=0.0.0.0 \
    --kube-proxy-arg metrics-bind-address=0.0.0.0 \
    --kube-scheduler-arg bind-address=0.0.0.0 \
    --tls-san=my-k8s.priv.kuurstra.com
```

Bekijk token:

```shell
sudo cat /var/lib/rancher/k3s/server/node-token
```

### Installeer k3s op overige controllers

Verwerk de token in onderstaand commando en voer deze uit op alle nodes die aan het cluster gejoined moeten worden:

```shell
export K3S_TOKEN={{TOKEN}}
```

```shell
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.27.11+k3s1 sh -s - server \
    --server https://192.168.2.100 \
    --service-cidr=10.201.0.0/16 \
    --disable=coredns \
    --disable=traefik \
    --disable=servicelb \
    --flannel-backend none  \
    --disable-network-policy \
    --disable-kube-proxy \
    --cluster-dns=10.201.0.2 \
    --cluster-domain=k8s.kuurstra.com \
    --node-taint CriticalAddonsOnly=true:NoExecute \
    --data-dir=/var/lib/rancher/k3s \
    --etcd-expose-metrics \
    --kube-controller-manager-arg bind-address=0.0.0.0 \
    --kube-proxy-arg metrics-bind-address=0.0.0.0 \
    --kube-scheduler-arg bind-address=0.0.0.0 \
    --write-kubeconfig=/etc/rancher/k3s/k3s.yaml
```

### Installeer agents

```bash
sudo mkdir -p /etc/rancher/k3s
sudo vi /etc/rancher/k3s/k3s.yaml
sudo chmod 600 /etc/rancher/k3s/k3s.yaml
```

```shell
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.27.11+k3s1 sh -s - agent \
    --server https://192.168.2.100
```

## 1password

```shell
OP_CONNECT_CREDENTIALS=$( cat 1password-credentials.json | base64 )
kubectl -n kube-system create secret generic op-credentials \
  --from-literal=op-session=$OP_CONNECT_CREDENTIALS
kubectl -n kube-system create secret generic onepassword-token \
  --from-literal=token=$OP_CONNECT_TOKEN
```

## Install critical

```shell
cd ~/code/gitlab/mkuurstra/my-k3s/apps/cilium/overlays/my-k3s
k kustomize . --enable-helm --load-restrictor=LoadRestrictionsNone  | k apply -f -
sleep 5
k kustomize . --enable-helm --load-restrictor=LoadRestrictionsNone  | k apply -f -

# Restart pods
kubectl get pods --all-namespaces -o custom-columns=NAMESPACE:.metadata.namespace,NAME:.metadata.name,HOSTNETWORK:.spec.hostNetwork --no-headers=true | grep '<none>' | awk '{print "-n "$1" "$2}' | xargs -L 1 -r kubectl delete pod

cd ~/code/gitlab/mkuurstra/my-k3s/apps/coredns/overlays/my-k3s
k kustomize . --enable-helm --load-restrictor=LoadRestrictionsNone  | k apply -f -

cd ~/code/gitlab/mkuurstra/my-k3s/apps/1password/overlays/my-k3s
k kustomize . --enable-helm --load-restrictor=LoadRestrictionsNone  | k apply -f -

cd ~/code/gitlab/mkuurstra/my-k3s/cluster-config/overlays/my-k3s
k kustomize . --enable-helm --load-restrictor=LoadRestrictionsNone  | k apply -f -

cd ~/code/gitlab/mkuurstra/my-k3s/apps/cert-manager/overlays/my-k3s
k kustomize . --enable-helm --load-restrictor=LoadRestrictionsNone  | k apply -f -

cd ~/code/gitlab/mkuurstra/my-k3s/apps/traefik/overlays/my-k3s
k kustomize . --enable-helm --load-restrictor=LoadRestrictionsNone  | k apply -f -

cd ~/code/gitlab/mkuurstra/my-k3s/apps/longhorn/overlays/my-k3s
k kustomize . --enable-helm --load-restrictor=LoadRestrictionsNone  | k apply -f -
sleep 5
k kustomize . --enable-helm --load-restrictor=LoadRestrictionsNone  | k apply -f -
```

Restore storage

```shell
cd ~/code/gitlab/mkuurstra/my-k3s/apps/argo-cd/overlays/my-k3s
k kustomize . --enable-helm --load-restrictor=LoadRestrictionsNone  | k apply -f -
sleep 5
k kustomize . --enable-helm --load-restrictor=LoadRestrictionsNone  | k apply -f -
```
