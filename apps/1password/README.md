# Application 1password

## Initial secret

Credentials need to be double base64 encoded: [link](https://1password.community/discussion/131378/loadlocalauthv2-failed-to-credentialsdatafrombase64#:~:text=tl%3Bdr%20the%201password%2Dcredentials.json%20file%20needs%20to%20be%20double%20base64%20encoded%20in%20the%20secret%20data!%20%F0%9F%A4%A6%F0%9F%8F%BB%E2%80%8D%E2%99%82%EF%B8%8F)

```bash
OP_CONNECT_CREDENTIALS=$( cat 1password-credentials.json | base64 )

kubectl -n kube-system create secret generic op-credentials \
  --from-literal=op-session=$OP_CONNECT_CREDENTIALS
```

```bash
OP_CONNECT_TOKEN=""

kubectl -n kube-system create secret generic onepassword-token \
  --from-literal=token=$OP_CONNECT_TOKEN
```
