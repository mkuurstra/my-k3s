bpf:
  masquerade: true

k8sNetworkPolicy:
  enabled: false

nodePort:
  enabled: true
externalIPs:
  enabled: true
hostPort:
  enabled: true

envoy:
  enabled: false

envoyConfig:
  # -- Enable CiliumEnvoyConfig CRD
  # CiliumEnvoyConfig CRD can also be implicitly enabled by other options.
  enabled: true

  # -- SecretsNamespace is the namespace in which envoy SDS will retrieve secrets from.
  secretsNamespace:
    # -- Create secrets namespace for CiliumEnvoyConfig CRDs.
    create: true

    # -- The name of the secret namespace to which Cilium agents are given read access.
    name: cilium-secrets

ingressController:
  # -- Enable cilium ingress controller
  # This will automatically set enable-envoy-config as well.
  enabled: true

  # -- Cilium allows you to specify load balancer mode for the Ingress resource
  # dedicated: The Ingress controller will create a dedicated loadbalancer for the Ingress.
  # shared: The Ingress controller will use a shared loadbalancer for all Ingress resources.
  loadbalancerMode: shared

  service:
    # -- Configure a specific nodePort for insecure HTTP traffic on the shared LB service
    insecureNodePort: 30080
    # -- Configure a specific nodePort for secure HTTPS traffic on the shared LB service
    secureNodePort: 30443

# -- Specify which network interfaces can run the eBPF datapath. This means
# that a packet sent from a pod to a destination outside the cluster will be
# masqueraded (to an output device IPv4 address), if the output device runs the
# program. When not specified, probing will automatically detect devices that have
# a non-local route. This should be used only when autodetection is not suitable.
# devices: ""


l2announcements:
  enabled: true

# -- Configure the client side rate limit for the agent and operator
#
# If the amount of requests to the Kubernetes API server exceeds the configured
# rate limit, the agent and operator will start to throttle requests by delaying
# them until there is budget or the request times out.
k8sClientRateLimit:
  # -- (int) The sustained request rate in requests per second.
  # @default -- 5 for k8s up to 1.26. 10 for k8s version 1.27+
  qps: 50
  # -- (int) The burst request rate in requests per second.
  # The rate limiter will allow short bursts with a higher rate.
  # @default -- 10 for k8s up to 1.26. 20 for k8s version 1.27+
  burst: 200

# -- Roll out cilium agent pods automatically when configmap is updated.
rollOutCiliumPods: true

operator:
  prometheus:
    enabled: true
    serviceMonitor:
      enabled: true

  # -- Roll out cilium-operator pods automatically when configmap is updated.
  rollOutPods: true

  # -- Affinity for cilium-operator
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: "node-role.kubernetes.io/control-plane"
            operator: "Exists"

  # -- Node tolerations for cilium-operator scheduling to nodes with taints
  # ref: https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/
  tolerations:
  - key: "CriticalAddonsOnly"
    operator: "Exists"
  - key: "node-role.kubernetes.io/master"
    operator: "Exists"
    effect: "NoSchedule"
  - key: "node-role.kubernetes.io/controlplane"
    operator: "Exists"
    effect: "NoSchedule"
  - key: "node-role.kubernetes.io/control-plane"
    operator: "Exists"
    effect: "NoSchedule"
  - key: "node-role.kubernetes.io/etcd"
    operator: "Exists"
    effect: "NoExecute"

# -- (string) Kubernetes config path
# @default -- `"~/.kube/config"`
kubeConfigPath: "/etc/rancher/k3s/k3s.yaml"
# -- (string) Kubernetes service port
k8sServicePort: "443"

hubble:
  metrics:
    enabled:
    - dns:query;ignoreAAAA
    - drop
    - tcp
    - flow
    - icmp
    - http
    enableOpenMetrics: true
    serviceMonitor:
      enabled: true
    dashboards:
      enabled: true
      namespace: monitoring
  # -- Enable Hubble (true by default).
  enabled: true
  relay:
    prometheus:
      enabled: true
      serviceMonitor:
        enabled: true
    enabled: true
    # -- Roll out Hubble Relay pods automatically when configmap is updated.
    rollOutPods: true
  ui:
    enabled: true
    # -- Roll out Hubble-ui pods automatically when configmap is updated.
    rollOutPods: true

ipam:
  # -- Configure IP Address Management mode.
  # ref: https://docs.cilium.io/en/stable/network/concepts/ipam/
  mode: "cluster-pool"
  # -- Maximum rate at which the CiliumNode custom resource is updated.
  ciliumNodeUpdateRate: "15s"
  operator:
    # -- IPv4 CIDR list range to delegate to individual nodes for IPAM.
    clusterPoolIPv4PodCIDRList: [ "10.200.0.0/16" ]
    # -- IPv4 CIDR mask size to delegate to individual nodes for IPAM.
    clusterPoolIPv4MaskSize: 24

ipv4:
  # -- Enable IPv4 support.
  enabled: true

ipv6:
  # -- Enable IPv6 support.
  enabled: false

# -- Configure the kube-proxy replacement in Cilium BPF datapath
# Valid options are "true", "false", "disabled" (deprecated), "partial" (deprecated), "strict" (deprecated).
# ref: https://docs.cilium.io/en/stable/network/kubernetes/kubeproxy-free/
kubeProxyReplacement: "true"

prometheus:
  enabled: true
  serviceMonitor:
    enabled: true
    trustCRDsExist: true
