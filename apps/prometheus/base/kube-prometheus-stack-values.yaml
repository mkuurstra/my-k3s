---
kubelet:
  serviceMonitor:
    ## RelabelConfigs to apply to samples before scraping
    ## ref: https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/api.md#relabelconfig
    ##
    cAdvisorRelabelings:
    ## metrics_path is required to match upstream rules and charts
    - action: replace
      sourceLabels: [ __metrics_path__ ]
      targetLabel: metrics_path
    - action: replace
      targetLabel: "nodename"
      sourceLabels:
      - "node"
    - action: replace
      replacement: my-k3s
      targetLabel: cluster

defaultRules:
  ## Disabled PrometheusRule alerts
  disabled:
    Watchdog: true

grafana:
  enabled: false
  ## ForceDeployDatasources Create datasource configmap even if grafana deployment has been disabled
  ##
  forceDeployDatasources: true
  ## ForceDeployDashboard Create dashboard configmap even if grafana deployment has been disabled
  ##
  forceDeployDashboards: true

  ## Timezone for the default dashboards
  ## Other options are: browser or a specific timezone, i.e. Europe/Luxembourg
  ##
  defaultDashboardsTimezone: Europe/Amsterdam

additionalPrometheusRulesMap:
  alerting-rules.yml:
    groups:
    - name: Heartbeats
      rules:
      - alert: Prometheus heartbeat
        expr: vector(1)
        labels:
          origin: k3s
          Environment: Production
          event: Heartbeat
          type: Heartbeat
          instance: prometheus
          monitor: prometheus
          severity: major
          timeout: "120"
        annotations:
          description: 'This is an alert meant to ensure that the entire alerting pipeline is functional. This alert is always firing, therefore it should always be firing in Alertmanager and always fire against a receiver. There are integrations with various notification mechanisms that send a notification when this alert is not firing. For example the "DeadMansSnitch" integration in PagerDuty.'
          runbook_url: https://runbooks.prometheus-operator.dev/runbooks/general/watchdog
          summary: An alert that should always be firing to certify that Alertmanager is working properly.

    - name: Backup alerts
      rules:
      - alert: VeleroBackupPartialFailures
        annotations:
          message: Velero backup {{ $labels.schedule }} has {{ $value | humanizePercentage }} partialy failed backups.
        expr: |-
          velero_backup_partial_failure_total{schedule!=""} / velero_backup_attempt_total{schedule!=""} > 0.25
        for: 15m
        labels:
          severity: warning
      - alert: VeleroBackupFailures
        annotations:
          message: Velero backup {{ $labels.schedule }} has {{ $value | humanizePercentage }} failed backups.
        expr: |-
          velero_backup_failure_total{schedule!=""} / velero_backup_attempt_total{schedule!=""} > 0.25
        for: 15m
        labels:
          severity: warning

    - name: longhorn.rules
      rules:
      - alert: LonghornVolumeActualSpaceUsedWarning
        annotations:
          description: The actual space used by Longhorn volume {{$labels.volume}} on {{$labels.nodename}} is at {{$value | humanize }}% capacity for more than 5 minutes.
          summary: The actual used space of Longhorn volume is over 200% of the capacity.
        expr: (longhorn_volume_actual_size_bytes / longhorn_volume_capacity_bytes) * 100 > 150
        for: 5m
        labels:
          issue: The actual used space of Longhorn volume {{$labels.volume}} on {{$labels.nodename}} is high.
          severity: warning
      - alert: LonghornVolumeStatusCritical
        annotations:
          description: Longhorn volume {{$labels.volume}} on {{$labels.nodename}} is Fault for more than 2 minutes.
          summary: Longhorn volume {{$labels.volume}} is Fault
        expr: longhorn_volume_robustness == 3
        for: 5m
        labels:
          issue: Longhorn volume {{$labels.volume}} is Fault.
          severity: critical
      - alert: LonghornVolumeStatusWarning
        annotations:
          description: Longhorn volume {{$labels.volume}} on {{$labels.nodename}} is Degraded for more than 5 minutes.
          summary: Longhorn volume {{$labels.volume}} is Degraded
        expr: longhorn_volume_robustness == 2
        for: 5m
        labels:
          issue: Longhorn volume {{$labels.volume}} is Degraded.
          severity: warning
      - alert: LonghornNodeStorageWarning
        annotations:
          description: The used storage of node {{$labels.nodename}} is at {{$value}}% capacity for more than 5 minutes.
          summary: The used storage of node is over 90% of the capacity.
        expr: (longhorn_node_storage_usage_bytes / longhorn_node_storage_capacity_bytes) * 100 > 90
        for: 5m
        labels:
          issue: The used storage of node {{$labels.nodename}} is high.
          severity: critical
      - alert: LonghornNodeDown
        annotations:
          description: There are {{$value}} Longhorn nodes which have been offline for more than 5 minutes.
          summary: Longhorn nodes is offline
        expr: (avg(longhorn_node_count_total) or on() vector(0)) - (count(longhorn_node_status{condition="ready"} == 1) or on() vector(0)) > 0
        for: 5m
        labels:
          issue: There are {{$value}} Longhorn nodes are offline
          severity: critical
      - alert: LonghornIntanceManagerCPUUsageWarning
        annotations:
          description: Longhorn instance manager {{$labels.instance_manager}} on {{$labels.nodename}} has CPU Usage / CPU request is {{$value}}% for more than 5 minutes.
          summary: Longhorn instance manager {{$labels.instance_manager}} on {{$labels.nodename}} has CPU Usage / CPU request is over 300%.
        expr: (longhorn_instance_manager_cpu_usage_millicpu/longhorn_instance_manager_cpu_requests_millicpu) * 100 > 300
        for: 5m
        labels:
          issue: Longhorn instance manager {{$labels.instance_manager}} on {{$labels.nodename}} consumes 3 times the CPU request.
          severity: warning
      - alert: LonghornNodeCPUUsageWarning
        annotations:
          description: Longhorn node {{$labels.nodename}} has CPU Usage / CPU capacity is {{$value}}% for more than 5 minutes.
          summary: Longhorn node {{$labels.nodename}} experiences high CPU pressure for more than 5m.
        expr: (longhorn_node_cpu_usage_millicpu / longhorn_node_cpu_capacity_millicpu) * 100 > 90
        for: 5m
        labels:
          issue: Longhorn node {{$labels.nodename}} experiences high CPU pressure.
          severity: warning

##
global:
  rbac:
    create: true

## Configuration for alertmanager
## ref: https://prometheus.io/docs/alerting/alertmanager/
##
alertmanager:
  ## Deploy alertmanager
  ##
  enabled: true

  ## Annotations for Alertmanager
  ##
  annotations: {}

  ## Configure pod disruption budgets for Alertmanager
  ## ref: https://kubernetes.io/docs/tasks/run-application/configure-pdb/#specifying-a-poddisruptionbudget
  ## This configuration is immutable once created and will require the PDB to be deleted to be changed
  ## https://github.com/kubernetes/kubernetes/issues/45398
  ##
  podDisruptionBudget:
    enabled: false
    minAvailable: 1
    maxUnavailable: ""

  ## Alertmanager configuration directives
  ## ref: https://prometheus.io/docs/alerting/configuration/#configuration-file
  ##      https://prometheus.io/webtools/alerting/routing-tree-editor/
  ##
  config:
    global:
      resolve_timeout: 5m
    inhibit_rules:
    - source_matchers:
      - "severity = critical"
      target_matchers:
      - "severity =~ warning|info"
      equal:
      - "namespace"
      - "alertname"
    - source_matchers:
      - "severity = warning"
      target_matchers:
      - "severity = info"
      equal:
      - "namespace"
      - "alertname"
    - source_matchers:
      - "alertname = InfoInhibitor"
      target_matchers:
      - "severity = info"
      equal:
      - "namespace"
    route:
      receiver: alerts
      group_by: [ "namespace" ]
      group_wait: 30s
      group_interval: 5m
      repeat_interval: 1h
      routes:
      - receiver: "null"
        match:
          alertname: InfoInhibitor
      - receiver: heartbeat
        group_by: [ "..." ]
        # Send every minute repeat_interval + group_interval
        group_interval: 30s
        repeat_interval: 30s
        match:
          event: Heartbeat
    receivers:
    - name: "null"
    - name: heartbeat
      # webhook_configs:
      # - url: 'https://status.priv.kuurstra.com/api/webhooks/prometheus'
      #   send_resolved: false
      #   http_config:
      #     authorization:
      #       type: Key
      #       credentials_file: /etc/alertmanager/secrets/op-prometheus/alerta-key
    - name: alerts
      # webhook_configs:
      # - url: 'https://status.priv.kuurstra.com/api/webhooks/prometheus'
      #   send_resolved: true
      #   http_config:
      #     authorization:
      #       type: Key
      #       credentials_file: /etc/alertmanager/secrets/op-prometheus/alerta-key
      slack_configs:
      - channel: "#prometheus"
        send_resolved: true
        api_url_file: /etc/alertmanager/secrets/op-prometheus/slack-api-url

        color: '{{ template "slack.color" . }}'
        title: '{{ template "slack.title" . }}'
        text: '{{ template "slack.text" . }}'
        actions:
        - type: button
          text: "Runbook :green_book:"
          url: "{{ (index .Alerts 0).Annotations.runbook_url }}"
        - type: button
          text: "Query :mag:"
          url: "{{ (index .Alerts 0).GeneratorURL }}"
        - type: button
          text: "Dashboard :chart_with_upwards_trend:"
          url: "{{ (index .Alerts 0).Annotations.dashboard_url }}"
        # - type: button
        #   text: 'Silence :no_bell:'
        #   url: '{{ template "__alert_silence_link" . }}'
    templates:
    - "/etc/alertmanager/config/*.tmpl"

  ## Alertmanager configuration directives (as string type, preferred over the config hash map)
  ## stringConfig will be used only, if tplConfig is true
  ## ref: https://prometheus.io/docs/alerting/configuration/#configuration-file
  ##      https://prometheus.io/webtools/alerting/routing-tree-editor/
  ##
  stringConfig: ""

  ## Pass the Alertmanager configuration directives through Helm's templating
  ## engine. If the Alertmanager configuration contains Alertmanager templates,
  ## they'll need to be properly escaped so that they are not interpreted by
  ## Helm
  ## ref: https://helm.sh/docs/developing_charts/#using-the-tpl-function
  ##      https://prometheus.io/docs/alerting/configuration/#tmpl_string
  ##      https://prometheus.io/docs/alerting/notifications/
  ##      https://prometheus.io/docs/alerting/notification_examples/
  tplConfig: false

  ## Alertmanager template files to format alerts
  ## By default, templateFiles are placed in /etc/alertmanager/config/ and if
  ## they have a .tmpl file suffix will be loaded. See config.templates above
  ## to change, add other suffixes. If adding other suffixes, be sure to update
  ## config.templates above to include those suffixes.
  ## ref: https://prometheus.io/docs/alerting/notifications/
  ##      https://prometheus.io/docs/alerting/notification_examples/
  ##
  templateFiles:
    alertmanager.tmpl: |-
      {{/* Alertmanager Silence link */}}
      {{ define "__alert_silence_link" -}}
          {{ .ExternalURL }}/#/silences/new?filter=%7B
          {{- range .CommonLabels.SortedPairs -}}
              {{- if ne .Name "alertname" -}}
                  {{- .Name }}%3D"{{- .Value -}}"%2C%20
              {{- end -}}
          {{- end -}}
          alertname%3D"{{- .CommonLabels.alertname -}}"%7D
      {{- end }}

      {{/* Severity of the alert */}}
      {{ define "__alert_severity" -}}
          {{- if eq .CommonLabels.severity "critical" -}}
          *Severity:* `Critical`
          {{- else if eq .CommonLabels.severity "warning" -}}
          *Severity:* `Warning`
          {{- else if eq .CommonLabels.severity "info" -}}
          *Severity:* `Info`
          {{- else -}}
          *Severity:* :question: {{ .CommonLabels.severity }}
          {{- end }}
      {{- end }}

      {{/* Title of the Slack alert */}}
      {{ define "slack.title" -}}
        [{{ .Status | toUpper -}}
        {{ if eq .Status "firing" }}:{{ .Alerts.Firing | len }}{{- end -}}
        ] {{ .CommonLabels.alertname }}
      {{- end }}

      {{/* Color of Slack attachment (appears as line next to alert )*/}}
      {{ define "slack.color" -}}
          {{ if eq .Status "firing" -}}
              {{ if eq .CommonLabels.severity "warning" -}}
                  warning
              {{- else if eq .CommonLabels.severity "critical" -}}
                  danger
              {{- else -}}
                  #439FE0
              {{- end -}}
          {{ else -}}
          good
          {{- end }}
      {{- end }}

      {{/* The text to display in the alert */}}
      {{ define "slack.text" -}}

          {{ template "__alert_severity" . }}
          {{- if (index .Alerts 0).Annotations.summary }}
          {{- "\n" -}}
          *Summary:* {{ (index .Alerts 0).Annotations.summary }}
          {{- end }}

          {{ range .Alerts }}

              {{- if .Annotations.description }}
              {{- "\n" -}}
              {{ .Annotations.description }}
              {{- "\n" -}}
              {{- end }}
              {{- if .Annotations.message }}
              {{- "\n" -}}
              {{ .Annotations.message }}
              {{- "\n" -}}
              {{- end }}

          {{- end }}

      {{- end }}

  ingress:
    enabled: true

    # For Kubernetes >= 1.18 you should specify the ingress-controller via the field ingressClassName
    # See https://kubernetes.io/blog/2020/04/02/improvements-to-the-ingress-api-in-kubernetes-1.18/#specifying-the-class-of-an-ingress
    ingressClassName: traefik

    annotations:
      traefik.ingress.kubernetes.io/router.entrypoints: websecure
      cert-manager.io/cluster-issuer: letsencrypt-production
      traefik.ingress.kubernetes.io/router.middlewares: traefik-system-internal-only@kubernetescrd

    ## Hosts must be provided if Ingress is enabled.
    ##
    hosts:
    - alertmanager.priv.kuurstra.com
    ## Paths to use for ingress rules - one path should match the alertmanagerSpec.routePrefix
    ##
    paths:
    - /
    ## For Kubernetes >= 1.18 you should specify the pathType (determines how Ingress paths should be matched)
    ## See https://kubernetes.io/blog/2020/04/02/improvements-to-the-ingress-api-in-kubernetes-1.18/#better-path-matching-with-path-types
    # pathType: ImplementationSpecific

    ## TLS configuration for Alertmanager Ingress
    ## Secret must be manually created in the namespace
    ##
    tls:
    - secretName: alertmanager-general-tls
      hosts:
      - alertmanager.priv.kuurstra.com

  ## Settings affecting alertmanagerSpec
  ## ref: https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/api.md#alertmanagerspec
  ##
  alertmanagerSpec:
    ## Define Log Format
    # Use logfmt (default) or json logging
    logFormat: json

    secrets:
    - op-prometheus
    ## Define resources requests and limits for single Pods.
    ## ref: https://kubernetes.io/docs/user-guide/compute-resources/
    ##
    resources: {}
    # requests:
    #   memory: 400Mi

    ## Containers allows injecting additional containers. This is meant to allow adding an authentication proxy to an Alertmanager pod.
    ##
    containers: []
    # containers:
    # - name: oauth-proxy
    #   image: quay.io/oauth2-proxy/oauth2-proxy:v7.5.1
    #   args:
    #   - --upstream=http://127.0.0.1:9093
    #   - --http-address=0.0.0.0:8081
    #   - --metrics-address=0.0.0.0:8082
    #   - ...
    #   ports:
    #   - containerPort: 8081
    #     name: oauth-proxy
    #     protocol: TCP
    #   - containerPort: 8082
    #     name: oauth-metrics
    #     protocol: TCP
    #   resources: {}

    storage:
      volumeClaimTemplate:
        spec:
          storageClassName: longhorn-trivial-2
          accessModes: [ "ReadWriteOnce" ]
          resources:
            requests:
              storage: 5Gi
    ## Deploy node exporter as a daemonset to all nodes
    ##

nodeExporter:
  enabled: true
  operatingSystems:
    linux:
      enabled: true
    darwin:
      enabled: true

## Configuration for Configuration for kube-state-metrics subchart subchart
##
kube-state-metrics:
  # Remove scrape annotation, causing double metrics with ServiceMonitor
  prometheusScrape: false

  prometheus:
    monitor:
      relabelings:
      - action: replace
        replacement: my-k3s
        targetLabel: cluster
      - action: replace
        targetLabel: "nodename"
        sourceLabels:
        - "__meta_kubernetes_pod_node_name"

## Configuration for prometheus-node-exporter subchart
##
prometheus-node-exporter:
  tolerations:
  - key: CriticalAddonsOnly
    operator: Exists

  service:
    # Remove scrape annotation, causing double metrics with ServiceMonitor
    annotations:
      prometheus.io/scrape: "false"

  prometheus:
    monitor:
      ## RelabelConfigs to apply to samples before scraping
      ## ref: https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/api.md#relabelconfig
      ##
      relabelings:
      - action: replace
        sourceLabels: [ __meta_kubernetes_pod_node_name ]
        separator: ;
        regex: ^(.*)$
        targetLabel: nodename
        replacement: $1
      - action: replace
        replacement: my-k3s
        targetLabel: cluster

## Manages Prometheus and Alertmanager components
##
prometheusOperator:
  enabled: true
  admissionWebhooks:
    enabled: false

    # Use certmanager to generate webhook certs
    certManager:
      enabled: true

  ## Number of old replicasets to retain ##
  ## The default value is 10, 0 will garbage-collect old replicasets ##
  revisionHistoryLimit: 10

  ## Namespaces to scope the interaction of the Prometheus Operator and the apiserver (allow list).
  ## This is mutually exclusive with denyNamespaces. Setting this to an empty object will disable the configuration
  ##
  namespaces: {}
    # releaseNamespace: true
    # additional:
    # - kube-system

  ## Namespaces not to scope the interaction of the Prometheus Operator (deny list).
  ##
  denyNamespaces: []

  ## Filter namespaces to look for prometheus-operator custom resources
  ##
  alertmanagerInstanceNamespaces: []
  alertmanagerConfigNamespaces: []
  prometheusInstanceNamespaces: []
  thanosRulerInstanceNamespaces: []

  ## Resource limits & requests
  ##
  resources: {}
  # limits:
  #   cpu: 200m
  #   memory: 200Mi
  # requests:
  #   cpu: 100m
  #   memory: 100Mi

  ## Prometheus-config-reloader
  ##
  prometheusConfigReloader:
    # add prometheus config reloader liveness and readiness probe. Default: false
    # enableProbe: true

    # resource config for prometheusConfigReloader
    resources: {}
      # requests:
      #   cpu: 200m
      #   memory: 50Mi
      # limits:
      #   cpu: 200m
      #   memory: 50Mi

## Deploy a Prometheus instance
##
prometheus:
  enabled: true

  serviceMonitor:
    relabelings:
    - action: replace
      sourceLabels: [ __meta_kubernetes_pod_node_name ]
      separator: ;
      regex: ^(.*)$
      targetLabel: nodename
      replacement: $1
    - action: replace
      replacement: my-k3s
      targetLabel: cluster

  ingress:
    enabled: true

    # For Kubernetes >= 1.18 you should specify the ingress-controller via the field ingressClassName
    # See https://kubernetes.io/blog/2020/04/02/improvements-to-the-ingress-api-in-kubernetes-1.18/#specifying-the-class-of-an-ingress
    ingressClassName: traefik

    annotations:
      traefik.ingress.kubernetes.io/router.entrypoints: websecure
      cert-manager.io/cluster-issuer: letsencrypt-production
      traefik.ingress.kubernetes.io/router.middlewares: traefik-system-internal-only@kubernetescrd

    ## Hostnames.
    ## Must be provided if Ingress is enabled.
    ##
    hosts:
    - prometheus.priv.kuurstra.com
    ## Paths to use for ingress rules - one path should match the prometheusSpec.routePrefix
    ##
    paths:
    - /
    ## TLS configuration for Prometheus Ingress
    ## Secret must be manually created in the namespace
    ##
    tls:
    - secretName: prometheus-general-tls
      hosts:
      - prometheus.priv.kuurstra.com

  ## Settings affecting prometheusSpec
  ## ref: https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/api.md#prometheusspec
  ##
  prometheusSpec:
    ## EnableAdminAPI enables Prometheus the administrative HTTP API which includes functionality such as deleting time series.
    ## This is disabled by default.
    ## ref: https://prometheus.io/docs/prometheus/latest/querying/api/#tsdb-admin-apis
    ##
    enableAdminAPI: true

    ## If true, a nil or {} value will cause the
    ## prometheus resource to be created with selectors based on values in the helm deployment,
    ##
    ruleSelectorNilUsesHelmValues: false
    serviceMonitorSelectorNilUsesHelmValues: false
    podMonitorSelectorNilUsesHelmValues: false
    probeSelectorNilUsesHelmValues: false
    scrapeConfigSelectorNilUsesHelmValues: false

    ## External URL at which Prometheus will be reachable.
    ##
    externalUrl: "https://prometheus.priv.kuurstra.com"

    ## Name of the external label used to denote replica name
    ##
    replicaExternalLabelName: prometheus_replica

    ## Name of the external label used to denote Prometheus instance name
    ##
    prometheusExternalLabelName: prometheus

    ## How long to retain metrics
    ##
    retention: 1d

    ## Allow out-of-order/out-of-bounds samples ingested into Prometheus for a specified duration
    ## See https://prometheus.io/docs/prometheus/latest/configuration/configuration/#tsdb
    tsdb:
      outOfOrderTimeWindow: 0s

    ## Log format for Prometheus be configured in
    ##
    logFormat: json

    ## Resource limits & requests
    ##
    resources: {}
    # requests:
    #   memory: 400Mi

    ## Prometheus StorageSpec for persistent data
    ## ref: https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/user-guides/storage.md
    ##
    storageSpec:
      ## Using PersistentVolumeClaim
      ##
      volumeClaimTemplate:
        spec:
          # selector:
          #   matchLabels:
          #     app.kubernetes.io/name: my-example-prometheus
          storageClassName: longhorn-trivial-2
          accessModes: [ "ReadWriteOnce" ]
          resources:
            requests:
              storage: 50Gi

    ## Using tmpfs volume
    ##
    #  emptyDir:
    #    medium: Memory

    ## AdditionalScrapeConfigs allows specifying additional Prometheus scrape configurations. Scrape configurations
    ## are appended to the configurations generated by the Prometheus Operator. Job configurations must have the form
    ## as specified in the official Prometheus documentation:
    ## https://prometheus.io/docs/prometheus/latest/configuration/configuration/#scrape_config. As scrape configs are
    ## appended, the user is responsible to make sure it is valid. Note that using this feature may expose the possibility
    ## to break upgrades of Prometheus. It is advised to review Prometheus release notes to ensure that no incompatible
    ## scrape configs are going to break Prometheus after the upgrade.
    ## AdditionalScrapeConfigs can be defined as a list or as a templated string.
    ##
    additionalScrapeConfigs:
    # Scrape config for service endpoints.
    #
    # The relabeling allows the actual service scrape endpoint to be configured
    # via the following annotations:
    #
    # * `prometheus.io/scrape`: Only scrape services that have a value of
    # `true`, except if `prometheus.io/scrape-slow` is set to `true` as well.
    # * `prometheus.io/scheme`: If the metrics endpoint is secured then you will need
    # to set this to `https` & most likely set the `tls_config` of the scrape config.
    # * `prometheus.io/path`: If the metrics path is not `/metrics` override this.
    # * `prometheus.io/port`: If the metrics are exposed on a different port to the
    # service then set this appropriately.
    # * `prometheus.io/param_<parameter>`: If the metrics endpoint uses parameters
    # then you can set any parameter
    - job_name: "kubernetes-service-endpoints"
      honor_labels: true

      kubernetes_sd_configs:
      - role: endpoints

      relabel_configs:
      - source_labels: [ __meta_kubernetes_service_annotation_prometheus_io_scrape ]
        action: keep
        regex: true
      - source_labels: [ __meta_kubernetes_service_annotation_prometheus_io_scrape_slow ]
        action: drop
        regex: true
      - source_labels: [ __meta_kubernetes_service_annotation_prometheus_io_scheme ]
        action: replace
        target_label: __scheme__
        regex: (https?)
      - source_labels: [ __meta_kubernetes_service_annotation_prometheus_io_path ]
        action: replace
        target_label: __metrics_path__
        regex: (.+)
      - source_labels: [ __address__, __meta_kubernetes_service_annotation_prometheus_io_port ]
        action: replace
        target_label: __address__
        regex: (.+?)(?::\d+)?;(\d+)
        replacement: $1:$2
      - action: labelmap
        regex: __meta_kubernetes_service_annotation_prometheus_io_param_(.+)
        replacement: __param_$1
      - action: labelmap
        regex: __meta_kubernetes_service_label_(.+)
      - source_labels: [ __meta_kubernetes_namespace ]
        action: replace
        target_label: namespace
      - source_labels: [ __meta_kubernetes_service_name ]
        action: replace
        target_label: service
      - source_labels: [ __meta_kubernetes_pod_node_name ]
        action: replace
        target_label: nodename

    # Scrape config for slow service endpoints; same as above, but with a larger
    # timeout and a larger interval
    #
    # The relabeling allows the actual service scrape endpoint to be configured
    # via the following annotations:
    #
    # * `prometheus.io/scrape-slow`: Only scrape services that have a value of `true`
    # * `prometheus.io/scheme`: If the metrics endpoint is secured then you will need
    # to set this to `https` & most likely set the `tls_config` of the scrape config.
    # * `prometheus.io/path`: If the metrics path is not `/metrics` override this.
    # * `prometheus.io/port`: If the metrics are exposed on a different port to the
    # service then set this appropriately.
    # * `prometheus.io/param_<parameter>`: If the metrics endpoint uses parameters
    # then you can set any parameter
    - job_name: "kubernetes-service-endpoints-slow"
      honor_labels: true

      scrape_interval: 5m
      scrape_timeout: 30s

      kubernetes_sd_configs:
      - role: endpoints

      relabel_configs:
      - source_labels: [ __meta_kubernetes_service_annotation_prometheus_io_scrape_slow ]
        action: keep
        regex: true
      - source_labels: [ __meta_kubernetes_service_annotation_prometheus_io_scheme ]
        action: replace
        target_label: __scheme__
        regex: (https?)
      - source_labels: [ __meta_kubernetes_service_annotation_prometheus_io_path ]
        action: replace
        target_label: __metrics_path__
        regex: (.+)
      - source_labels: [ __address__, __meta_kubernetes_service_annotation_prometheus_io_port ]
        action: replace
        target_label: __address__
        regex: (.+?)(?::\d+)?;(\d+)
        replacement: $1:$2
      - action: labelmap
        regex: __meta_kubernetes_service_annotation_prometheus_io_param_(.+)
        replacement: __param_$1
      - action: labelmap
        regex: __meta_kubernetes_service_label_(.+)
      - source_labels: [ __meta_kubernetes_namespace ]
        action: replace
        target_label: namespace
      - source_labels: [ __meta_kubernetes_service_name ]
        action: replace
        target_label: service
      - source_labels: [ __meta_kubernetes_pod_node_name ]
        action: replace
        target_label: nodename

    - job_name: "prometheus-pushgateway"
      honor_labels: true

      kubernetes_sd_configs:
      - role: service

      relabel_configs:
      - source_labels: [ __meta_kubernetes_service_annotation_prometheus_io_probe ]
        action: keep
        regex: pushgateway

    # Example scrape config for probing services via the Blackbox Exporter.
    #
    # The relabeling allows the actual service scrape endpoint to be configured
    # via the following annotations:
    #
    # * `prometheus.io/probe`: Only probe services that have a value of `true`
    - job_name: "kubernetes-services"
      honor_labels: true

      metrics_path: /probe
      params:
        module: [ http_2xx ]

      kubernetes_sd_configs:
      - role: service

      relabel_configs:
      - source_labels: [ __meta_kubernetes_service_annotation_prometheus_io_probe ]
        action: keep
        regex: true
      - source_labels: [ __address__ ]
        target_label: __param_target
      - target_label: __address__
        replacement: blackbox
      - source_labels: [ __param_target ]
        target_label: instance
      - action: labelmap
        regex: __meta_kubernetes_service_label_(.+)
      - source_labels: [ __meta_kubernetes_namespace ]
        target_label: namespace
      - source_labels: [ __meta_kubernetes_service_name ]
        target_label: service

    # Example scrape config for pods
    #
    # The relabeling allows the actual pod scrape endpoint to be configured via the
    # following annotations:
    #
    # * `prometheus.io/scrape`: Only scrape pods that have a value of `true`,
    # except if `prometheus.io/scrape-slow` is set to `true` as well.
    # * `prometheus.io/scheme`: If the metrics endpoint is secured then you will need
    # to set this to `https` & most likely set the `tls_config` of the scrape config.
    # * `prometheus.io/path`: If the metrics path is not `/metrics` override this.
    # * `prometheus.io/port`: Scrape the pod on the indicated port instead of the default of `9102`.
    - job_name: "kubernetes-pods"
      honor_labels: true

      kubernetes_sd_configs:
      - role: pod

      relabel_configs:
      - source_labels: [ __meta_kubernetes_pod_annotation_prometheus_io_scrape ]
        action: keep
        regex: true
      - source_labels: [ __meta_kubernetes_pod_annotation_prometheus_io_scrape_slow ]
        action: drop
        regex: true
      - source_labels: [ __meta_kubernetes_pod_annotation_prometheus_io_scheme ]
        action: replace
        regex: (https?)
        target_label: __scheme__
      - source_labels: [ __meta_kubernetes_pod_annotation_prometheus_io_path ]
        action: replace
        target_label: __metrics_path__
        regex: (.+)
      - source_labels: [ __meta_kubernetes_pod_annotation_prometheus_io_port, __meta_kubernetes_pod_ip ]
        action: replace
        regex: (\d+);(([A-Fa-f0-9]{1,4}::?){1,7}[A-Fa-f0-9]{1,4})
        replacement: "[$2]:$1"
        target_label: __address__
      - source_labels: [ __meta_kubernetes_pod_annotation_prometheus_io_port, __meta_kubernetes_pod_ip ]
        action: replace
        regex: (\d+);((([0-9]+?)(\.|$)){4})
        replacement: $2:$1
        target_label: __address__
      - action: labelmap
        regex: __meta_kubernetes_pod_annotation_prometheus_io_param_(.+)
        replacement: __param_$1
      - action: labelmap
        regex: __meta_kubernetes_pod_label_(.+)
      - source_labels: [ __meta_kubernetes_namespace ]
        action: replace
        target_label: namespace
      - source_labels: [ __meta_kubernetes_pod_name ]
        action: replace
        target_label: pod
      - source_labels: [ __meta_kubernetes_pod_phase ]
        regex: Pending|Succeeded|Failed|Completed
        action: drop
      - source_labels: [ __meta_kubernetes_pod_node_name ]
        action: replace
        target_label: nodename

    # Example Scrape config for pods which should be scraped slower. An useful example
    # would be stackriver-exporter which queries an API on every scrape of the pod
    #
    # The relabeling allows the actual pod scrape endpoint to be configured via the
    # following annotations:
    #
    # * `prometheus.io/scrape-slow`: Only scrape pods that have a value of `true`
    # * `prometheus.io/scheme`: If the metrics endpoint is secured then you will need
    # to set this to `https` & most likely set the `tls_config` of the scrape config.
    # * `prometheus.io/path`: If the metrics path is not `/metrics` override this.
    # * `prometheus.io/port`: Scrape the pod on the indicated port instead of the default of `9102`.
    - job_name: "kubernetes-pods-slow"
      honor_labels: true

      scrape_interval: 5m
      scrape_timeout: 30s

      kubernetes_sd_configs:
      - role: pod

      relabel_configs:
      - source_labels: [ __meta_kubernetes_pod_annotation_prometheus_io_scrape_slow ]
        action: keep
        regex: true
      - source_labels: [ __meta_kubernetes_pod_annotation_prometheus_io_scheme ]
        action: replace
        regex: (https?)
        target_label: __scheme__
      - source_labels: [ __meta_kubernetes_pod_annotation_prometheus_io_path ]
        action: replace
        target_label: __metrics_path__
        regex: (.+)
      - source_labels: [ __meta_kubernetes_pod_annotation_prometheus_io_port, __meta_kubernetes_pod_ip ]
        action: replace
        regex: (\d+);(([A-Fa-f0-9]{1,4}::?){1,7}[A-Fa-f0-9]{1,4})
        replacement: "[$2]:$1"
        target_label: __address__
      - source_labels: [ __meta_kubernetes_pod_annotation_prometheus_io_port, __meta_kubernetes_pod_ip ]
        action: replace
        regex: (\d+);((([0-9]+?)(\.|$)){4})
        replacement: $2:$1
        target_label: __address__
      - action: labelmap
        regex: __meta_kubernetes_pod_annotation_prometheus_io_param_(.+)
        replacement: __param_$1
      - action: labelmap
        regex: __meta_kubernetes_pod_label_(.+)
      - source_labels: [ __meta_kubernetes_namespace ]
        action: replace
        target_label: namespace
      - source_labels: [ __meta_kubernetes_pod_name ]
        action: replace
        target_label: pod
      - source_labels: [ __meta_kubernetes_pod_phase ]
        regex: Pending|Succeeded|Failed|Completed
        action: drop
      - source_labels: [ __meta_kubernetes_pod_node_name ]
        action: replace
        target_label: nodename

    - job_name: minio-job
      bearer_token_file: /etc/prometheus/secrets/op-prometheus/minio-scrape-token
      metrics_path: /minio/v2/metrics/cluster
      scheme: http
      static_configs:
      - targets: [ "192.168.30.5:9000" ]
    ## Secrets is a list of Secrets in the same namespace as the Prometheus object, which shall be mounted into the Prometheus Pods.
    ## The Secrets are mounted into /etc/prometheus/secrets/. Secrets changes after initial creation of a Prometheus object are not
    ## reflected in the running Pods. To change the secrets mounted into the Prometheus Pods, the object must be deleted and recreated
    ## with the new list of secrets.
    ##
    secrets:
    - op-prometheus
    ## If additional scrape configurations are already deployed in a single secret file you can use this section.
    ## Expected values are the secret name and key
    ## Cannot be used with additionalScrapeConfigs
    additionalScrapeConfigsSecret: {}
      # enabled: false
      # name:
      # key:

    ## AdditionalAlertRelabelConfigs allows specifying Prometheus alert relabel configurations. Alert relabel configurations specified are appended
    ## to the configurations generated by the Prometheus Operator. Alert relabel configurations specified must have the form as specified in the
    ## official Prometheus documentation: https://prometheus.io/docs/prometheus/latest/configuration/configuration/#alert_relabel_configs.
    ## As alert relabel configs are appended, the user is responsible to make sure it is valid. Note that using this feature may expose the
    ## possibility to break upgrades of Prometheus. It is advised to review Prometheus release notes to ensure that no incompatible alert relabel
    ## configs are going to break Prometheus after the upgrade.
    ##
    additionalAlertRelabelConfigs: []
    # - separator: ;
    #   regex: prometheus_replica
    #   replacement: $1
    #   action: labeldrop

    ## If additional alert relabel configurations are already deployed in a single secret, or you want to manage
    ## them separately from the helm deployment, you can use this section.
    ## Expected values are the secret name and key
    ## Cannot be used with additionalAlertRelabelConfigs
    additionalAlertRelabelConfigsSecret: {}
      # name:
      # key:

    ## Thanos configuration allows configuring various aspects of a Prometheus server in a Thanos environment.
    ## This section is experimental, it may change significantly without deprecation notice in any release.
    ## This is experimental and may change significantly without backward compatibility in any release.
    ## ref: https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/api.md#thanosspec
    ##
    thanos: {}
      # secretProviderClass:
      #   provider: gcp
      #   parameters:
      #     secrets: |
      #       - resourceName: "projects/$PROJECT_ID/secrets/testsecret/versions/latest"
      #         fileName: "objstore.yaml"
      # objectStorageConfigFile: /var/secrets/object-store.yaml

    ## Containers allows injecting additional containers. This is meant to allow adding an authentication proxy to a Prometheus pod.
    ## if using proxy extraContainer update targetPort with proxy container port
    containers: []
    # containers:
    # - name: oauth-proxy
    #   image: quay.io/oauth2-proxy/oauth2-proxy:v7.5.1
    #   args:
    #   - --upstream=http://127.0.0.1:9090
    #   - --http-address=0.0.0.0:8081
    #   - --metrics-address=0.0.0.0:8082
    #   - ...
    #   ports:
    #   - containerPort: 8081
    #     name: oauth-proxy
    #     protocol: TCP
    #   - containerPort: 8082
    #     name: oauth-metrics
    #     protocol: TCP
    #   resources: {}

    ## Setting to true produces cleaner resource names, but requires a data migration because the name of the persistent volume changes. Therefore this should only be set once on initial installation.
    ##
cleanPrometheusOperatorObjectNames: true
