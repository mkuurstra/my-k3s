---
customConfig:
  sources:
    journal_logs:
      type: vector
      address: 0.0.0.0:6000
      version: "2"
    kubernetes_logs:
      type: vector
      address: 0.0.0.0:6010
      version: "2"
    sidecar_logs:
      type: vector
      address: 0.0.0.0:6020
      version: "2"
    vector_metrics:
      type: internal_metrics
    syslog_mikrotik:
      type: syslog
      address: 0.0.0.0:5140
      mode: udp
  transforms:
    mikrotik_remap:
      type: remap
      inputs:
      - syslog_mikrotik
      source: |
        .original_timestamp = .timestamp
        .timestamp = now()

        if contains(string!(.message), "connection-state") {
          structured, err = parse_regex(.message, r'(?x)^  # Extended mode to ignore whitespace
            in:(?P<in_interface>[^,]+)\s
            out:(?P<out_interface>[^,]+),\s
            connection-state:[^\s,]+
            (,snat|,dnat)?
            (\ssrc-mac\s(?P<src_mac>[A-z0-9:]+),)?
            \sproto\s
            (?P<protocol>[^\s,]+)
            (\s\((?P<protocol_options>[^)]+)\))?,\s
            (  # Sources and destination for SNAT
              (?P<snat_source>[0-9.]+)(:(?P<snat_source_port>\d+))?
              ->
              (?P<snat_destination>[0-9.]+)(:(?P<snat_destination_port>\d+))?
              ,\sNAT\s
              \(
              [0-9.]+(:\d+)?
              ->
              (?P<translated_source>[0-9.]+)(:(?P<translated_source_port>\d+))?
              \)
              ->
              [0-9.]+(:\d+)?
            )?
            (  # Sources and destination for DNAT
              (?P<dnat_source>[0-9.]+)(:(?P<dnat_source_port>\d+))?
              ->
              (?P<translated_destination>[0-9.]+)(:(?P<translated_destination_port>\d+))?
              ,\sNAT\s
              [0-9.]+(:\d+)?
              ->\(
              (?P<dnat_destination>[0-9.]+)(:(?P<dnat_destination_port>\d+))?->
              [0-9.]+(:\d+)?
              \)
            )?
            (  # Sources and destination for normal connection
              (?P<source>[0-9.]+)(:(?P<source_port>\d+))?
              ->
              (?P<destination>[0-9.]+)(:(?P<destination_port>\d+))?
            )?
            (,\sprio\s\d+->\d+)?
            ,\slen\s\d+
            $
          ')
          if err != null {
            log("Unable to parse regex: " + err, level: "error")
            log("unparsable: " + to_string!(.message))
            abort
          }

          .in_interface = structured.in_interface
          .out_interface = structured.out_interface
          .src_mac = structured.src_mac
          .protocol = structured.protocol
          .protocol_options = structured.protocol_options

          # IP adresses can be multiple values in case of normal or NAT connections
          .source = structured.source || structured.snat_source || structured.dnat_source
          .destination = structured.destination || structured.snat_destination || structured.dnat_destination

          # Porst can can be multiple values in case of normal or NAT connections but are always integer
          source_port = structured.source_port || structured.snat_source_port || structured.dnat_source_port || 0
          .source_port = to_int!(source_port)
          destination_port = structured.destination_port || structured.snat_destination_port || structured.dnat_destination_port || 0
          .destination_port = to_int!(destination_port)

          # Other fields in case of NAT
          .translated_source = structured.translated_source
          .translated_source_port = to_int(structured.translated_source_port) ?? 0
          .translated_destination = structured.translated_destination
          .translated_destination_port = to_int(structured.translated_destination_port) ?? 0

          # Add a log type field for easy filtering
          .log_type = "firewall"

          # Remove source ip, shows k8s agent IP
          # I think its because of metallb Load Balancing
          del(.source_ip)
        }
    # opnsense_remap:
    #   type: remap
    #   inputs:
    #     - opnsense_source
    #   source: |
    #     msg = parse_csv!(string!(.message))
    #     # Only parse IPv4 / IPv6
    #     if msg[8] == "4" || msg[8] == "6" {
    #       .filter_interface = msg[4]
    #       .filter_direction = msg[7]
    #       .filter_action = msg[6]
    #       .filter_ip_version = msg[8]
    #       .filter_protocol = msg[16]
    #       .filter_source_ip = msg[18]
    #       .filter_destination_ip = msg[19]
    #       if (msg[16] == "icmp" || msg[16] == "igmp" || msg[16] == "gre") {
    #         .filter_data = msg[20]
    #       } else {
    #         .filter_source_port = msg[20]
    #         .filter_destination_port = msg[21]
    #         .filter_data_length = msg[22]
    #         if msg[8] == "4" && msg[16] == "tcp" {
    #           .filter_tcp_flags = msg[23]
    #         }
    #       }
    #     }
    # opnsense_route:
    #   type: route
    #   inputs:
    #     - opnsense_remap
    #   route:
    #     pass_action: .filter_action == "pass"
  sinks:
    loki_journal:
      type: loki
      inputs:
      - journal_logs
      endpoint: http://loki-gateway.monitoring.svc.k8s.kuurstra.com:80
      encoding:
        codec: logfmt
      batch:
        max_bytes: 2049000
      out_of_order_action: accept
      remove_label_fields: true
      remove_timestamp: true
      labels:
        hostname: >-
          {{`{{ host }}`}}
        source_type: journald
    loki_kubernetes:
      type: loki
      inputs:
      - kubernetes_logs
      endpoint: http://loki-gateway.monitoring.svc.k8s.kuurstra.com:80
      encoding:
        codec: logfmt
      batch:
        max_bytes: 2049000
      out_of_order_action: accept
      remove_label_fields: true
      remove_timestamp: true
      labels:
        app: >-
          {{`{{ custom_app_name }}`}}
        source_type: >-
          {{`{{ source_type }}`}}
        cluster: >-
          {{`{{ cluster_name }}`}}
        namespace: >-
          {{`{{ pod_namespace }}`}}
        pod: >-
          {{`{{ pod_name }}`}}
        container: >-
          {{`{{ container_name }}`}}
        nodename: >-
          {{`{{ pod_node_name }}`}}
    loki_sidecar:
      type: loki
      inputs:
      - sidecar_logs
      endpoint: http://loki-gateway.monitoring.svc.k8s.kuurstra.com:80
      encoding:
        codec: logfmt
      batch:
        max_bytes: 2049000
      out_of_order_action: accept
      remove_label_fields: true
      remove_timestamp: true
      labels:
        source_type: >-
          {{`{{ source_type }}`}}
        app: >-
          {{`{{ app }}`}}
    prom_exporter:
      type: prometheus_exporter
      inputs:
      - vector_metrics
      address: 0.0.0.0:9090
    mikrotik:
      type: loki
      inputs:
      - mikrotik_remap
      endpoint: http://loki-gateway.monitoring.svc.k8s.kuurstra.com:80
      encoding:
        codec: logfmt
      batch:
        max_bytes: 2049000
      labels:
        source_type: syslog
      out_of_order_action: accept

service:
  loadBalancerIP: 192.168.2.195
